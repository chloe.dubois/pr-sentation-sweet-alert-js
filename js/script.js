bouton1 = document.getElementById('bouton1');
bouton2 = document.getElementById('bouton2');
bouton3 = document.getElementById('bouton3');
bouton4 = document.getElementById('bouton4');
bouton5 = document.getElementById('bouton5');
bouton6 = document.getElementById('bouton6');
bouton7 = document.getElementById('bouton7');
bouton8 = document.getElementById('bouton8');
bouton9 = document.getElementById('bouton9');
bouton10 = document.getElementById('bouton10');

document.addEventListener('DOMContentLoaded', (event) => {
    bouton1.addEventListener('click', (event) => {
        alert('Hello! I\'m ugly!');
    });
    bouton2.addEventListener('click', (event) => {
        swal('Hello!', 'I\'m so sweeeeeet!', 'success');
    });
    bouton3.addEventListener('click', (event) => {
        swal("Bravo!", "Vous êtes inscrits", "success");
    });
    bouton4.addEventListener('click', (event) => {
        swal('Attention!', 'Vous êtes sur le point de supprimer un fichier', 'warning');
    });
    bouton5.addEventListener('click', (event) => {
        swal('Oups!', 'Une erreur s\'est produite', 'error');
    });
    bouton6.addEventListener('click', (event) => {
        swal('Info', 'Il va faire chaud aujourd\'hui', 'info');
    });
    bouton7.addEventListener('click', (event) => {
      swal({
        title: "Êtes-vous sûrs ?",
        text: "Une fois supprimé, le fichier ne sera plus disponible.",
        icon: "warning",
        buttons: ["Euh non...", "Mais oui!"],
      });
    });

    bouton8.addEventListener('click', (event) => {
      swal("Cliquez sur le bouton ou en dehors du modal.")
    .then((value) => {
     swal(`La valeur renvoyée est: ${value}`);
    });
    });

    bouton9.addEventListener('click', (event) => {
        swal({
            title: "Êtes-vous sûr?",
            text: "Une fois supprimé, vous ne pourrez plus récupérer ce fichier imaginaire!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Votre fichier imaginaire a été supprimé!", {
                icon: "success",
              });
            } else {
              swal("Votre fichier imaginaire est en sécurité!");
            }
          });       
    });

    bouton10.addEventListener('click', (event) => {

          swal({
            text: 'Search for a movie. e.g. "La La Land".',
            content: "input",
            button: {
              text: "Search!",
              closeModal: false,
            },
          })
          .then(name => {
            if (!name) throw null;
            
            return fetch(`https://itunes.apple.com/search?term=${name}&entity=movie`);
          })
          .then(results => {
            return results.json();
          })
          .then(json => {
            const movie = json.results[0];
            
            if (!movie) {
              return swal("No movie was found!");
            }
            
            const name = movie.trackName;
            const imageURL = movie.artworkUrl100;
            
            swal({
              title: "Top result:",
              text: name,
              icon: imageURL,
            });
          })
          .catch(err => {
            if (err) {
              swal("Oh noes!", "The AJAX request failed!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
        });
       
});


